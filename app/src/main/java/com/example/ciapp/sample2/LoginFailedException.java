package com.example.ciapp.sample2;

public class LoginFailedException extends Exception {
    public LoginFailedException(String msg){
        super(msg);
    }

    public static class UserNotFoundException extends Exception {
        public UserNotFoundException(String msg){
            super(msg);
        }
    }
}
